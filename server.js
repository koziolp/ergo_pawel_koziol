let express = require('express');
let	basicAuth = require('express-basic-auth');
let app = express();
let morgan = require('morgan');
let bodyParser = require('body-parser');
let port = 8080;
let claim = require('./backend/routes/claim');

app.use(morgan('combined')); //'combined' outputs the Apache style LOGs

var noAuth = function(req, res, next) {
return next();
};
                                     
app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));  

app.use(express.static(__dirname + '/webapp'));

app.get('/claim', noAuth, function(req, res) {
  res.sendFile(__dirname + '/webapp/claim_form.html');
});

app.route("/api/claims")
    .post(claim.createClaim);

app.use(basicAuth({
    users: { 'user': 'pass', 'testuser': 'testpass'},
    challenge: true,
    realm: 'InsClmDashboard'
}))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/webapp/index.html');
});

app.route("/api/claims")
    .get(claim.getClaims);
    
app.route("/api/claims/:claimId/status")
    .post(claim.updateClaimStatus);

app.listen(port);
console.log("Listening on port " + port);

module.exports = app;