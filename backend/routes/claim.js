let loki = require('lokijs');
let Chance = require('chance'),

chance = new Chance();

var db = new loki('claims.json')

var claims = db.addCollection('claims')

const CLAIM_DEFAULT_STATUS = "New";

/*
 * GET /claim route to retrieve all the claims.
 */
function getClaims(req, res) {
    res.json(claims.data);
}

/*
 * POST /claim to save a new claim.
 */
function createClaim(req, res) {
	//TODO add some validation
	let claim = req.body;
	claim.id = getRandomId();
	claim.status = CLAIM_DEFAULT_STATUS;
	let err = false;
	claims.insert(claim);
    res.json({message: "Claim successfully added.", claim });
}

function getRandomId() {
	return chance.natural();
}

/*
 * POST /claims/:id/status route to update the status of a claim
 */
function updateClaimStatus(req, res) {
	let claimId = parseInt(req.params.claimId);
	console.log("claimId: " + claimId);
	let claim = claims.find({'id': {'$eq': claimId}})[0];

	console.log("updating claim id: " + claim.id);
	let statusPayload = req.body;
    claim.status = statusPayload.status;
    claims.update(claim);
    res.json({message: "Claim status successfully updated.", claim });
}

//export all the functions
module.exports = { getClaims, createClaim, updateClaimStatus};