const React = require('react');
const ReactDOM = require('react-dom')
require('whatwg-fetch')

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {claims: []};
	}
	
	refreshState() {
		fetch('/api/claims', {headers: {Accept: 'application/json'},
                                                 credentials: 'same-origin'})
		.then(res => res.json())
		.then(json => this.setState({claims: json}))
	}
	
	componentDidMount() {
		this.refreshState()
  		this.interval = setInterval(() => this.refreshState(), 1000);
	}
	
	componentWillUnmount() {
 		 clearInterval(this.interval);
	}

	render() {
		return (
			<ClaimsList claims={this.state.claims}/>
		)
	}
}

class ClaimsList extends React.Component{
	render() {
		let claims = this.props.claims.map(claim =>
			<Claim key={claim.id} claim={claim}/>
		);
		return (
			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<th>E-mail</th>
						<th>Policy id</th>
						<th>Claim type</th>
						<th>Claim amount</th>
						<th>Date occured</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					{claims}
				</tbody>
			</table>
		)
	}
}

class Claim extends React.Component{

	constructor(props) {
    super(props);
    this.approveClaim = this.approveClaim.bind(this);
    this.rejectClaim = this.rejectClaim.bind(this);
  }
  
	render() {
		return (
			<tr>
				<td>{this.props.claim.name}</td>
				<td>{this.props.claim.email}</td>
				<td>{this.props.claim.policyId}</td>
				<td>{this.props.claim.claimType}</td>
				<td>{this.props.claim.claimAmount}</td>
				<td>{this.props.claim.dateOccured}</td>
				<td>{this.props.claim.status}</td>
				<td data-id='claim-actions'>
					<button onClick={this.approveClaim}>Approved</button>
					<button onClick={this.rejectClaim}>Rejected</button>
				</td>
			</tr>
		)
	}
	
	approveClaim() {
		console.log("approved: " + this.props.claim.id);
		this.updateClaimStatus('Approved');
	}
	
	rejectClaim() {
		console.log("rejected: " + this.props.claim.id);
		this.updateClaimStatus('Rejected');
	}
	
	updateClaimStatus(newStatus) {
	 fetch('/api/claims/' + this.props.claim.id + '/status', 
 	 	{ method: 'POST',  credentials: 'same-origin',
 		 	headers: {
   	 		'Content-Type': 'application/json',
    	    Accept: 'application/json'},
 		 body: JSON.stringify({
		    status: newStatus,
		  })
	 })
  	  .then(function(res) {
  	      console.log(res.json);
 	   }).catch(function(err) {
        console.log(err);
       });
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
)
