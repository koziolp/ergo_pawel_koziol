const React = require('react');
const ReactDOM = require('react-dom')
require('whatwg-fetch');
  
class ClaimForm extends React.Component {
 getInitialState() {
    return {name: "", email: "", policyId: "", claimType: "lostBaggage", claimAmount: "", dateOccured: ""};
  }
  
  constructor(props) {
    super(props);
    this.state = {claimType: "lostBaggage",
    			  msg: "Provide claim information"};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
  	event.preventDefault();
  	console.log("state: " + this.state);
 	 fetch('/api/claims', 
 	 { method: 'POST', 
 	 	headers: {
    	'Content-Type': 'application/json'
 		 },
 		 body: JSON.stringify({
		    name: this.state.name,
		    email: this.state.email,
		    policyId: this.state.policyId,
		    claimType: this.state.claimType,
		    claimAmount: this.state.claimAmount,
		    dateOccured: this.state.dateOccured
		  })
	 })
  	  .then(function(res) {
  	      console.log(res.json);
  	       this.setState(this.getInitialState());
 	   }).catch(function(err) {
 	   //TODO display error msg
          console.log(err);
       });
       this.setState(this.getInitialState());
       this.setState({msg: "Thank you for your submission"});
  }

  render() {
    return (
    <div>
      <div class="form-style-1-heading">{this.state.msg}</div>
      <form onSubmit={this.handleSubmit} id="claimForm">
        <label>
          Name: </label>
          <input name="name" type="text" value={this.state.name} onChange={this.handleChange} />
           <label>
          E-mail: </label>
          <input name="email" type="text" value={this.state.email} onChange={this.handleChange} />
          <label>
          Policy id: </label>
          <input name="policyId" type="text" value={this.state.policyId} onChange={this.handleChange} />
           <label>
          Claim type </label>
          <select name="claimType" form="claimForm" value={this.state.claimType} onChange={this.handleChange}>
			  <option value="lostBaggage">Lost Baggage</option>
 			  <option value="theft">Theft</option>
  			  <option value="missedFlight">Missed Flight</option>Accident
  			  <option value="illness">Illness</option>
  			  <option value="accident">Accident</option>
		  </select>
           <label>
          Claim amount: </label>
          <input name="claimAmount" type="number" value={this.state.claimAmount} onChange={this.handleChange} />
           <label>
          Date occured: </label>
          <input name="dateOccured" type="date" value={this.state.dateOccured} onChange={this.handleChange} />
       
        <input type="submit" value="Submit" />
      </form>
    </div>
    );
  }
}

ReactDOM.render(
	<ClaimForm />,
	document.getElementById('claim_form')
)
