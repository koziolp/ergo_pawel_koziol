# Claims dashboard demo application

## Compilation

npm install

npm run-script webpack

## Running

npm start

Dashboard URL: http://localhost:8080/

basic authentication: 

user: user

password: pass

Claims input form URL: http://localhost:8080/claim

## Testing

npm test

## Not realized "Should have" requirements

See TODO.txt