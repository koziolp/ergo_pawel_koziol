var path = require('path');

module.exports = {
    entry: {
    	dashboard: './webapp/app.js',
    	claim: './webapp/claim_form.js'
    },
    devtool: 'sourcemaps',
    cache: true,
    debug: true,
    output: {
        path: __dirname,
		filename: "./webapp/built/[name].bundle.js",
    },
    module: {
        loaders: [
            {
                test: path.join(__dirname, '.'),
                exclude: /(node_modules)/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }
            },
            {test: /\.json$/, loader: 'json-loader'}
        ]
    }
};
