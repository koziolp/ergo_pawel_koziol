let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('Claims', () => {
//TODO remove items before each test case
  describe('/GET claims', () => {
      it('claims list should be empty at the beginning', (done) => {
        chai.request(server)
            .get('/api/claims')
            .auth('testuser', 'testpass')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
              done();
            });
      });
  });
  
  describe('/POST claim', () => {
      it('it should be possible to add a new claim', (done) => {
        let claim = {
            name: "New name",
        
        }
        chai.request(server)
            .post('/api/claims')
            .auth('testuser', 'testpass')
            .send(claim)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
              done();
            });
      });
  });
  
  describe('/POST claim', () => {
      it('when claim is added it should appear on the list', (done) => {
      let claimName = "New name";
        let claim = {
            name: claimName,
        }
        chai.request(server)
            .post('/api/claims')
            .auth('testuser', 'testpass')
            .send(claim)
        .then(function() {
        chai.request(server)
            .get('/api/claims')
            .auth('testuser', 'testpass')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(2);
                res.body[1].name.should.be.eql(claimName);
                console.log(res.body[0]);
              done();
            });
        });
        });
  });
});